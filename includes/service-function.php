<?php
if ($_SERVER['REMOTE_ADDR'] == '::1') {
    require_once( $_SERVER['DOCUMENT_ROOT'] . '/wp_doral/wp-load.php' );
} else {
    require_once( $_SERVER['DOCUMENT_ROOT'] . '/doralprohealth/wp-load.php' );
}
global $wp_query;

$datos = $_POST['datos'];
$posts = get_post($datos); ?>
<?php $args = array('post_type' => 'services', 'orderby' => 'ASC', 'post__in' => array($datos)); ?>
<a id="closemodal" onclick="closeModal()" class="pull-right close-custom-modal">x</a>
<?php query_posts($args); ?>
<?php while (have_posts()) : the_post(); ?>
<?php $url = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID())); ?>
<img src="<?php echo $url; ?>" longdesc="<?php echo get_the_title(); ?>" alt="<?php echo get_the_title(); ?>" />
<h2><?php the_title(); ?></h2>
<?php the_content(); ?>
<?php endwhile; wp_reset_query(); ?>
