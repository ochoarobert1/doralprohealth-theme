![alt tag](http://robertochoa.com.ve/wp-content/uploads/2015/10/repo-logo.jpg)

# Doral Pro Health Wordpress Custom Theme #

* Version: 1.0
* Design: [Robert Ochoa](http://www.robertochoa.com.ve/)
* Development: [Robert Ochoa](http://www.robertochoa.com.ve/)

Tema diseñado por [Robert Ochoa](http://www.robertochoa.com.ve/) para Doral Pro Health Este tema custom fue construido en su totalidad, pasando por su etapa de Wireframing, rearmado, version anterior e implementación en hosting externo.

### Componentes Principales ###

* Twitter Bootstrap 3.3.6
* Font Awesome 4.6.3
* jQuery Nicescroll 3.6.8



### Funciones Incluídas ###

* Custom Post Type:
* Custom Taxonomies:
* Wordpress Menu Structure
* Custom Metabox:
* Custom contact section.

### Plugins Requeridos ###

* Rilwis' Metabox
* Jetpack by Wordpress

### Instrucciones de Instalación ###

1. Instalar los plugins requeridos.

2. Activar los plugins requeridos.

3. Instalar el tema via FTP o por el instalador de themes de Wordpress via zip

### Contacto ###

Soporte Oficial para este tema:

Repo Owner: [Robert Ochoa](http://www.robertochoa.com.ve/)

Main Developer: [Robert Ochoa](http://www.robertochoa.com.ve/)
