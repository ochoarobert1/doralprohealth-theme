<?php
/**
 * Template Name: Contact / Contacto
 *
 * @package WordPress
 * @subpackage doralprohealth
 * @since doralprohealth 1.0
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>
<?php $defaultatts = array('class' => 'img-responsive'); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <section class="the-contact-container col-md-12">
            <div class="container">
                <div class="row">
                    <div class="contact-item col-md-6">
                        <h2>Nuestra Ubicación</h2>
                        <?php the_content(); ?>
                    </div>
                    <div class="contact-item col-md-6">
                        <h2>Déjanos un Mensaje</h2>
                        <?php $contact = get_post_meta(get_the_ID(), 'rw_contact_form', true); ?>
                        <?php if ($contact != '') { echo do_shortcode($contact); } ?>
                    </div>
                </div>
            </div>
        </section>
        <section class="the-map-section col-md-12 no-paddingl no-paddingr">
            <?php get_template_part('templates/map'); ?>
        </section>
    </div>
</main>
<?php get_footer(); ?>
