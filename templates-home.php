<?php
/**
 * Template Name: Home / Inicio
 *
 * @package WordPress
 * @subpackage doralprohealth
 * @since doralprohealth 1.0
 */
?>
<?php get_header(); ?>
<?php the_post(); ?>
<?php $defaultatts = array('class' => 'img-responsive'); ?>
<main class="container-fluid" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <?php $args = array('post_type' => 'sponsors', 'posts_per_page' => -1, 'order' => 'DESC', 'orderby' => 'date'); ?>
        <?php query_posts($args); ?>
        <?php if (have_posts()) : ?>
        <section class="partners-container col-md-12">
            <div class="container">
                <div class="row">
                    <div class="partners-slider col-md-12 no-paddingl no-paddingr owl-carousel owl-theme">
                        <?php while (have_posts()) : the_post(); ?>
                        <div class="partner-item item">
                            <a href="<?php echo get_post_meta(get_the_ID(), 'rw_sponsors_url'); ?>" target="_blank" title="<?php echo get_the_title(); ?>">
                                <?php the_post_thumbnail('full', $defaultatts);?>
                            </a>
                        </div>
                        <?php endwhile; wp_reset_query(); ?>
                    </div>
                </div>
            </div>
        </section>
        <?php endif; ?>
        <section class="main-home-container col-md-12">
            <div class="container">
                <div class="row">
                    <div class="main-home-section-container col-md-7 no-paddingl">
                        <h2 class="section-title">Quienes somos</h2>
                        <div class="main-home-info-container col-md-12 no-paddingl no-paddingr">
                            <div class="col-md-6 no-paddingl">
                                <div class="embed-responsive embed-responsive-16by9">
                                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/gJ9O0R8nJnA" frameborder="0" allowfullscreen></iframe>
                                </div>
                            </div>
                            <p>Doral Pro Health (DPH) es una fundación sin fines de lucro formada por profesionales en el campo de la medicina y la odontología que señaló la necesidad de servicios de salud en un sector específico de la población de escasos recursos en el condado Miami Dade.</p>
                            <p>La fundación fue creada con el fin de proporcionar educación, métodos de prevención y atención medica primaria, todo en colaboración directa con el Departamento de Salud de el estado de la Florida quien, con la finalidad de detectar enfermedades o problemas médicos y dentales, brinda apoyo para el tratamiento de forma continua a los que necesitan los servicios disponibles para niños y adultos de todas las edades</p>

                        </div>
                        <div class="clearfix"></div>
                        <h2 class="section-title">Servicios</h2>
                        <div class="main-home-info-container col-md-12 no-paddingl no-paddingr">
                            <?php $args = array('post_type' => 'services', 'posts_per_page' => -1, 'order' => 'DESC', 'orderby' => 'date'); ?>
                            <?php query_posts($args); ?>
                            <?php if (have_posts()) : ?>
                            <?php $i=1; $y=1; while (have_posts()) : the_post(); ?>
                            <div onclick="showService(<?php echo get_the_ID(); ?>)" class="home-services-item col-md-6 <?php if ($i == 1) { echo 'no-paddingl'; } else { echo 'no-paddingr'; } ?>">
                                <div class="main-home-services-item main-home-services-item-<?php echo $y; ?> col-md-12 no-paddingl no-paddingr">
                                    <h2><?php the_title(); ?></h2>
                                </div>
                            </div>
                            <?php $i++; $y++; if ($i > 2) { $i = 1; } if ($y > 6) { $y = 1; } endwhile; wp_reset_query(); ?>
                            <?php endif; ?>
                            <?php /* SERVICE BIG CONTAINTER */ ?>
                            <div class="service-big-container service-big-hidden col-md-12">
                                <div class="service-info-container col-md-6 col-md-offset-3">

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="main-home-aside-container col-md-5 no-paddingr">
                        <h2 class="section-title">Últimas Noticias</h2>
                        <div class="main-home-info-container col-md-12 no-paddingl no-paddingr">
                            <?php $args = array('post_type' => 'sliders', 'posts_per_page' => 4, 'order' => 'ASC', 'orderby' => 'date'); ?>
                            <?php query_posts($args); ?>
                            <?php if (have_posts()) : ?>
                            <div class="main-home-news-container col-md-12 no-paddingl no-paddingr">
                                <?php while (have_posts()) : the_post(); ?>
                                <div class="main-home-news-item col-md-12 no-paddingl no-paddingr">
                                    <div class="main-home-news-picture col-md-12 no-paddingl no-paddingr">
                                        <a href="<?php echo get_post_meta(get_the_ID(), 'rw_slider_url', true); ?>" title="<?php echo get_the_title(); ?>">
                                            <?php the_post_thumbnail('slider_img', $defaultatts); ?>
                                        </a>
                                    </div>
                                    <div class="main-home-news-title col-md-12 no-paddingl no-paddingr">
                                        <a href="<?php echo get_post_meta(get_the_ID(), 'rw_slider_url', true); ?>" title="<?php echo get_the_title(); ?>">
                                            <h2><?php the_title(); ?></h2>
                                        </a>
                                    </div>
                                </div>
                                <?php endwhile; wp_reset_query(); ?>
                            </div>
                            <?php endif; ?>
                            <div class="main-home-extra-info col-md-12 no-paddingl no-paddingr">
                                <div class="col-md-6 no-paddingl">
                                    <a href="<?php echo home_url('/volunteers')?>"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/btn-voluntario.png" alt="Conviertete en Voluntario" class="img-responsive"/></a>
                                    <a href="<?php echo home_url('/beneficiarios')?>"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/btn-patrocinante.png" alt="Conviertete en Voluntario" class="img-responsive" /></a>
                                    <a href="<?php echo home_url('/donations/donaciones/')?>"><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/btn-donacion.png" alt="Conviertete en Voluntario" class="img-responsive" /></a>
                                    <a href=""><img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/btn_actualizaciones.png" alt="Recibe nuestras Actualizaciones" class="img-responsive" /></a>
                                </div>
                                <div class="col-md-6 no-paddingr">
                                    <a class="twitter-timeline" data-height="400" data-dnt="true" data-link-color="#008b8b" href="https://twitter.com/DoralProHealth">Tweets by DoralProHealth</a> <script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</main>
<?php get_footer(); ?>
