<?php get_header(); ?>
<?php $defaultatts = array('class' => 'img-responsive'); ?>
<main class="container" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">
    <div class="row">
        <section class="page-article col-md-12 no-paddingl no-paddingr">
            <?php $i = 1; ?>
            <?php if (have_posts()): while (have_posts()) : the_post(); ?>
            <article id="post-<?php the_ID(); ?>" class="team-item col-md-12 no-paddingl no-paddingr <?php echo join(' ', get_post_class()); ?>" role="article">
                <div class="team-item-title col-md-12 no-paddingl no-paddingr">
                    <div class="col-md-10 no-paddingl"><h2 rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_title(); ?> - <?php echo get_post_meta(get_the_ID(), 'rw_rank_member', true); ?></h2></div>
                    <div class="col-md-2 no-paddingr text-right">
                        <?php $link = get_post_meta(get_the_ID(), 'rw_facebook_url', true); ?>
                        <?php if ($link != '') { ?>
                        <a href="<?php echo $link; ?>" target="_blank" title="Facebook Profile"><i class="fa fa-facebook"></i></a>
                        <?php } ?>
                        <?php $link2 = get_post_meta(get_the_ID(), 'rw_twitter_url', true); ?>
                        <?php if ($link2 != '') { ?>
                        <a href="<?php echo $link2; ?>" target="_blank" title="Twitter Profile"><i class="fa fa-twitter"></i></a>
                        <?php } ?>
                        <?php $link3 = get_post_meta(get_the_ID(), 'rw_instagram_url', true); ?>
                        <?php if ($link3 != '') { ?>
                        <a href="<?php echo $link3; ?>" target="_blank" title="Instagram Profile"><i class="fa fa-instagram"></i></a>
                        <?php } ?>
                    </div>
                </div>
                <?php if ($i === 1) { ?>
                <picture class="col-md-4">
                    <?php if ( has_post_thumbnail()) : ?>
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                        <?php the_post_thumbnail('full', $defaultatts); ?>
                    </a>
                    <?php else : ?>
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/no-img.jpg" alt="No img" class="img-responsive" />
                    </a>
                    <?php endif; ?>
                </picture>
                <div class="col-md-8">
                    <p><?php the_content(); ?></p>
                </div>
                <?php } else { ?>

                <div class="col-md-8">
                    <p><?php the_content(); ?></p>
                </div>
                <picture class="col-md-4">
                    <?php if ( has_post_thumbnail()) : ?>
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                        <?php the_post_thumbnail('full', $defaultatts); ?>
                    </a>
                    <?php else : ?>
                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/no-img.jpg" alt="No img" class="img-responsive" />
                    </a>
                    <?php endif; ?>
                </picture>

                <?php } ?>

                <div class="clearfix"></div>
                <hr>

            </article>
            <?php $i++; if ($i > 2) { $i = 1; } endwhile; ?>

            <?php else: ?>
            <article>
                <h2>Disculpe, su busqueda no arrojo ningun resultado</h2>
                <h3>Haga click <a href="<?php echo home_url('/'); ?>">aqui</a> para volver al inicio</h3>
            </article>
            <?php endif; ?>
        </section>
    </div>
</main>
<?php get_footer(); ?>


